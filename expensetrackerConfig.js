var expenseTrackerConfig = function(requestConfig, grunt){

	var config = {
         connect: configureConnect,
		watch: configureWatch,
		sass: configureSass,
        uglify: configureUglify,
        sync: configureSync
	};

    var availablePortNumber = 9000, availableLiveReloadPortNumber = 35729;
	var themeConfig = requestConfig.themeConfig;
    var isDebugMode = requestConfig.isDebugMode;

	var sites = [];

	for(var site in themeConfig.interface){
        var siteConfig = {
            portNumber: availablePortNumber,
            liveReloadPortNumber: availableLiveReloadPortNumber,
            site: site
        };
		sites.push(siteConfig);
        availablePortNumber++;
        availableLiveReloadPortNumber++;
	}
	
	var activeSites = sites.filter(function(currentSite){
		return !requestConfig.hasSite || currentSite.site == requestConfig.site;
	});

    function configureConnect() {
        var connectConfig = {};
        
        activeSites.forEach(function(currentSite){
            var defaultConfig = extendOptions({}, themeConfig.config, themeConfig.interface[currentSite.site].config || {});
            
            connectConfig[currentSite.site] = {
				options:{
					port: currentSite.portNumber,
                    hostname: '*',
                    open: true,
                    livereload: currentSite.liveReloadPortNumber,
                    base: {
                        path: 'assets/' + defaultConfig.themeName + '/' + currentSite.site,
                        options: {
                            index: 'html/pattern-library.html'
                        }
                    },
				}
			};
		});

        return connectConfig;
    }
    
	function configureWatch() {
		var watchConfig = {};

        activeSites.forEach(function(currentSite){
            var defaultConfig = extendOptions({}, themeConfig.config, themeConfig.interface[currentSite.site].config || {});
            
            if(!requestConfig.hasModule || requestConfig.module == 'css')
            {
                
                watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-css'] = {
                    options:{
                        spawn: false,
                        livereload: currentSite.liveReloadPortNumber
                    },
                    files: [defaultConfig.sassPath + '/**/*.{sass,scss}'],
                    tasks: ['sass:' + defaultConfig.themeName + '-' + currentSite.site + '-css']
                };
                
                 watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-font'] = {
                    options:{
                        spawn: false,
                        event: ['all'],
                        livereload: currentSite.liveReloadPortNumber
                    },
                    files: [defaultConfig.fontPath + '/**/*'],
                    tasks: ['sync:' + defaultConfig.themeName + '-' + currentSite.site + '-font']
                };
                
                watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-image'] = {
                    options:{
                        spawn: false,
                        event: ['all'],
                        livereload: currentSite.liveReloadPortNumber
                    },
                    files: [defaultConfig.imagePath + '/**/*'],
                    tasks: ['sync:' + defaultConfig.themeName + '-' + currentSite.site + '-image']
                };
                
                if(defaultConfig.internalDependencies !== undefined)
                {
                    defaultConfig.internalDependencies.forEach(function(path){
                    watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-css']
                            .files.push(path + '/sass/**/*.{sass,scss}');
                            
                    watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-font']
                            .files.push(path + '/fonts/**/*');
                    
                    watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-image']
                            .files.push(path + '/images/**/*');
                    });
                }
                
                if(defaultConfig.externalDependencies !== undefined)
                {
                    defaultConfig.externalDependencies.forEach(function(path){
                    watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-css']
                            .files.push(path + '/sass/**/*.{sass,scss}');
                    });
                }
            }
            
             if(!requestConfig.hasModule || requestConfig.module == 'js')
            {
                var jsConfig = grunt.file.readJSON(themeConfig.interface[currentSite.site].config.scriptPath);
                var fileToWatch = [];
                
                for(var file in jsConfig){
                    fileToWatch = fileToWatch.concat(jsConfig[file]);
                }
                watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-js'] = {
                    options:{
                        spawn: false,
                        livereload: currentSite.liveReloadPortNumber
                    },
                    files: [fileToWatch],
                    tasks: ['uglify:' + defaultConfig.themeName + '-' + currentSite.site + '-js']
                };                
            }
           
            if(!requestConfig.hasModule || requestConfig.module == 'html')
            { 
                watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-html'] = {
                    options:{
                        spawn: false,
                        livereload: currentSite.liveReloadPortNumber
                    },
                    files: [defaultConfig.htmlPath + '/**/*.html'],
                    tasks: ['sync:' + defaultConfig.themeName + '-' + currentSite.site + '-html']
                };
                
                watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-media'] = {
                    options:{
                        spawn: false,
                        event: ['all'],
                        livereload: currentSite.liveReloadPortNumber
                    },
                    files: [defaultConfig.mediaPath + '/**/*.*'],
                    tasks: ['sync:' + defaultConfig.themeName + '-' + currentSite.site + '-media']
                };
                
                if(defaultConfig.internalDependencies !== undefined)
                {
                    defaultConfig.internalDependencies.forEach(function(path){
                        watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-html']
                                .files.push(path + '/html/**/*');
                                        
                        watchConfig[defaultConfig.themeName + '-' + currentSite.site + '-media']
                                .files.push(path + '/media/**/*.*');
                    });
                }
            }
		});

		return watchConfig;
	}

	function configureSass() {
		var sassConfig = {};
		
		activeSites.forEach(function(currentSite){
			var defaultConfig = extendOptions({}, themeConfig.config, themeConfig.interface[currentSite.site].config || {});
            var destination = isDebugMode 
                        ? themeConfig.interface[currentSite.site].css
                        : themeConfig.interface[currentSite.site].css + '/release';

			sassConfig[defaultConfig.themeName + '-' + currentSite.site + '-css'] = {
				options:{
					outputStyle: isDebugMode ? 'expanded' : 'compressed'
				},
				files: [
					{
						expand: true,
						cwd: defaultConfig.sassPath,
						src: ['*.{sass,scss}'],
						dest: destination,
						ext: '.css'
					}
				],
			};
		});
        
		return sassConfig;
	}
    
    function configureUglify()
    {
        var uglifyConfig = {};
        
        activeSites.forEach(function(currentSite){
            var defaultConfig = extendOptions({}, themeConfig.config, themeConfig.interface[currentSite.site].config ||{});
            var jsConfig = grunt.file.readJSON(themeConfig.interface[currentSite.site].config.scriptPath);
            var destination = isDebugMode 
                        ? themeConfig.interface[currentSite.site].js
                        : themeConfig.interface[currentSite.site].js + '/release';
            var fileConfig = {};
            
            for(var file in jsConfig){
                fileConfig[destination + '/' + file + (isDebugMode ? '.js' : '.min.js')] = jsConfig[file];
            } 
                        
            uglifyConfig[defaultConfig.themeName + '-' + currentSite.site + '-js'] = {
				options:{
					compress: !isDebugMode,
                    mangle: !isDebugMode,
                    beautify: isDebugMode,
                    preserveComments : isDebugMode
				},
				files: fileConfig
			};           
        })
        return uglifyConfig;
    }
    
    function configureSync()
    {
        var syncConfig = {};
        
        activeSites.forEach(function(currentSite){
           var defaultConfig = extendOptions({}, themeConfig.config, themeConfig.interface[currentSite.site].config || {});
           function Config(cwd, destination, module){
               this.expand = true;
               this.cwd = cwd;
               this.src = ['**'];
               this.dest = isDebugMode 
                            ? destination 
                            : themeConfig.interface[currentSite.site].css + '/release/' + module;
           }
           console.log(themeConfig.interface[currentSite.site].css + '/release');
           syncConfig[defaultConfig.themeName + '-' + currentSite.site + '-font'] = {
               files: [ new Config(defaultConfig.fontPath, themeConfig.interface[currentSite.site].fonts, 'fonts')],
               updateAndDelete: isDebugMode
           }
           
           syncConfig[defaultConfig.themeName + '-' + currentSite.site + '-image'] = {
               files: [new Config(defaultConfig.imagePath, themeConfig.interface[currentSite.site].images, 'images')],
               updateAndDelete: isDebugMode
           }
           
           if(isDebugMode){
                syncConfig[defaultConfig.themeName + '-' + currentSite.site + '-html'] = {
                    files: [new Config(defaultConfig.htmlPath, themeConfig.interface[currentSite.site].html, 'html')],
                    updateAndDelete: false
                }
                
                syncConfig[defaultConfig.themeName + '-' + currentSite.site + '-media'] = {
                    files: [new Config(defaultConfig.mediaPath, themeConfig.interface[currentSite.site].media, 'media')],
                    updateAndDelete: isDebugMode
                }
           }
           
           if(defaultConfig.internalDependencies !== undefined)
                {
                    defaultConfig.internalDependencies.forEach(function(path){
                        syncConfig[defaultConfig.themeName + '-' + currentSite.site + '-font']
                            .files.push(new Config(path + '/fonts', themeConfig.interface[currentSite.site].fonts, 'fonts'));
                            
                        syncConfig[defaultConfig.themeName + '-' + currentSite.site + '-image']
                            .files.push(new Config(path + '/images', themeConfig.interface[currentSite.site].images, 'images'));
                        
                        if(isDebugMode){    
                            syncConfig[defaultConfig.themeName + '-' + currentSite.site + '-html']
                                .files.push(new Config(path + '/html', themeConfig.interface[currentSite.site].html, 'html'));
                            
                            syncConfig[defaultConfig.themeName + '-' + currentSite.site + '-media']
                                .files.push(new Config(path + '/media', themeConfig.interface[currentSite.site].media, 'media'));
                        }
                    });
                }
        });

        return syncConfig;
    }
    
	function extendOptions(){
        Array.prototype.toLowerCase = function(){
            this.forEach(function(element, index, array){
                array[index] = element.toLowerCase();
            });
            
            return this;
        };
        
		for(var i = 1; i < arguments.length; i++)
			for(var key in arguments[i])
				if(arguments[i].hasOwnProperty(key))
					arguments[0][key] = (arguments[i][key]).toLowerCase();
                 
		return arguments[0];
	}

	return config;

}

module.exports = expenseTrackerConfig;