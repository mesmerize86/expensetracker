import React from 'react'
import { Route, IndexRoute } from 'react-router'

import Index from '../index'
import Home from '../home'
import Signup from '../register'

import PatternLibrary from '../../pattern-library/pattern-library'
// UI Component
import UIComponents from '../../pattern-library/components/ui/ui-components'
import FormGroupComponent from '../../pattern-library/components/ui/form-group'
import ButtonComponent from '../../pattern-library/components/ui/button'

// JS Component
import JSComponents from '../../pattern-library/components/js/js-components'
import DocumentComponent from '../../pattern-library/components/js/document'

// SCSS Component
import SCSSComponents from '../../pattern-library/components/scss/scss-components'

// Design Component
import DesignComponent from '../../pattern-library/components/design/design-components'
import ColorElements from '../../pattern-library/components/design/color-elements'
import Typography from '../../pattern-library/components/design/typography'


import PageNotFound from '../page-not-found'

export default (
  <Route path="/" component={Index}>
    <IndexRoute component={Home} />
    <Route path="/signup" component={Signup} />
    <Route path="/pattern-library" component={PatternLibrary}>
      <IndexRoute component={DesignComponent} />
      <Route path="/components/design/design-components" component={DesignComponent}>
        <IndexRoute component={ColorElements} />
        <Route path="/components/design/color-elements" component={ColorElements} />
        <Route path="/components/design/typography" component={Typography} />
      </Route>
      <Route path="/components/ui/ui-components" component={UIComponents}>
        <IndexRoute component={FormGroupComponent} />
        <Route path="/components/ui/form-group" component={FormGroupComponent} />
        <Route path="/components/ui/button" component={ButtonComponent} />
      </Route>
      <Route path="/components/js/js-components" component={JSComponents}>
        <IndexRoute component={DocumentComponent} />
        <Route path="/components/js/document" component={DocumentComponent} />
      </Route>
      <Route path="/components/scss/scss-components" component={SCSSComponents}></Route>
    </Route>
    <Route path="*" component={PageNotFound} />
  </Route>
)
