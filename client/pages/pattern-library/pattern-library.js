import React from 'react'

import Header from './layout/header'
import Navigation from './layout/navigation'
import MainContent from './layout/main-content'
import Sidebar from './layout/sidebar'


class PatternLibrary extends React.Component {
  render () {
    return(
        <div className="patternLibraryPage Page">
          <div className="wrapper">
            <Header />
            <Navigation />
            <MainContent sidebarNav={this.props.children} />
          </div>
        </div>
    )
  }
}

export default PatternLibrary;
