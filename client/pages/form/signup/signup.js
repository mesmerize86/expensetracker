import React from 'react'
import FormGroup from '../../../components/form-group'
import ValidateInput from '../../../../server/shared/validation/signup'

class Signup extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      username: '',
      password: '',
      confirmPassword: '',
      firstName: '',
      lastName: '',
      email: '',
      secretQuestion: '',
      secretAnswer: '',
      errors : {}
    }
  }

  onChange(e){
      this.setState({
        [e.target.name] : e.target.value
      })
  }

  isValid(){
    const { errors, isValid } = ValidateInput(this.state)

    if(!isValid){
      this.setState({ errors })
    }
    return isValid
  }

  onSubmit(e){
    e.preventDefault()

    if(this.isValid()){
      console.log('success')
    }
  }

  render () {
    const { errors } = this.state
    return(
      <div>
        <h4>Sign Up</h4>
        <form onSubmit={this.onSubmit.bind(this)}>
          <FormGroup
            label='Username'
            value={this.state.username}
            field='username'
            onChange={this.onChange.bind(this)}
            error={errors.username}
          />
          <FormGroup
            label='Password'
            value={this.state.password}
            field='password'
            type='password'
            onChange={this.onChange.bind(this)}
            error={errors.password}
          />
          <FormGroup
            label='Confirm Password'
            value={this.state.confirmPassword}
            field='confirmPassword'
            type='password'
            onChange={this.onChange.bind(this)}
            error={errors.confirmPassword}
          />
          <FormGroup
            label='First Name'
            value={this.state.firstName}
            field='firstName'
            onChange={this.onChange.bind(this)}
            error={errors.firstName}
          />
          <FormGroup
            label='Last Name'
            value={this.state.lastName}
            field='lastName'
            onChange={this.onChange.bind(this)}
            error={errors.lastName}
          />
          <FormGroup
            label='Email'
            value={this.state.email}
            field='email'
            onChange={this.onChange.bind(this)}
            error={errors.email}
          />
          <FormGroup
            label='Secret Question'
            value={this.state.secretQuestion}
            field='secretQuestion'
            onChange={this.onChange.bind(this)}
            error={errors.secretQuestion}
          />
          <FormGroup
            label='Secret Answer'
            value={this.state.secretAnswer}
            field='secretAnswer'
            onChange={this.onChange.bind(this)}
            error={errors.secretAnswer}
          />
          <button type="submit" className="button button-primary button-block">Register</button>
        </form>
      </div>
    )
  }
}

export default Signup;
