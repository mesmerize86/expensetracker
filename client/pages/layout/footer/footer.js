import React from 'react'

class Footer extends React.Component {
  render () {
    return(
      <div className="Footer">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div className="text-center">
                <p>&copy; Copyright 2017| Expense Tracker</p>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

export default Footer;
