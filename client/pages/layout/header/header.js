import React from 'react'

import TopBar from './top-bar/top-bar'
import NavBar from './nav-bar/nav-bar'

class Header extends React.Component {
  render () {
    return(
      <header>
        <TopBar />
        <NavBar />
      </header>
    )
  }
}

export default Header
