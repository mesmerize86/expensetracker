import React, { PropTypes } from 'react'
import { Link } from 'react-router'

class NavBar extends React.Component {
  render () {
    return(
      <nav className="Nav">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <div className="nav nav-right">
                <Link to="/" className="nav-link">Home</Link>
                <Link to="/signup" className="nav-link">Register</Link>
                <a href="#" className="nav-link">Contact</a>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default NavBar;
