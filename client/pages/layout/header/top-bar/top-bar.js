import React, { PropTypes } from 'react'

class TopBar extends React.Component {
  render () {
    return(
      <div className="bar-top">
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              <span>Email: lasataya@gmail.com</span>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default TopBar;
