import React from 'react'
import LoginForm from './form/login/login'

class Home extends React.Component {
  render () {
    return(
      <section className="homePage Page">
        <div className="container">
          <div className="row">
            <div className="col-xs-8 col-xs-offset-4">
              <LoginForm />
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Home;
