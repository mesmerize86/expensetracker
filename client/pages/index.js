import React from 'react'
import Header from './layout/header/header'
import Footer from './layout/footer/footer'

class Index extends React.Component {
  render () {
    return(
      <div>
        <Header />
        { this.props.children }
        <Footer />
      </div>
    )
  }
}

export default Index
