import React from 'react'
import classNames from 'classnames'

const FormGroup = ({field, value, type, placeholder, label, onChange, error }) => {
  return(
    <div className="form-group">
      <label className='form-label'>{label}</label>
      <input
        type={type}
        value={value}
        name={field}
        placeholder={placeholder}
        className='form-control'
        onChange={onChange}
        />
      {error && <span className='help-block'>{error}</span>}
    </div>
  )
}

  FormGroup.PropTypes = {
      label : React.PropTypes.string.isRequired,
      value : React.PropTypes.string.isRequired,
      name : React.PropTypes.string.isRequired,
      placeholder : React.PropTypes.string,
      error: React.PropTypes.string
  }

  FormGroup.defaultProps = {
    type : 'text'
  }

  export default FormGroup
