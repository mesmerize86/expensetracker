import React from 'react'
import { Link } from 'react-router'

class Sidebar extends React.Component {
  render () {
    return(
      <div className="sidebar">
        <label>Design</label>
        <ul className="sidebar-nav">
          <li><Link to="/design/color-elements" className="sidebar-nav-link">Design Palette</Link></li>
          <li><Link to="color-elements" className="sidebar-nav-link">UI Colours</Link></li>
          <li><Link to="/design/typography" className="sidebar-nav-link">Typography</Link></li>
        </ul>
      </div>
    )
  }
}

export default Sidebar
