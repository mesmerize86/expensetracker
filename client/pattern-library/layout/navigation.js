import React from 'react'
import { Link } from 'react-router'
import classnames from 'classnames'


class Navigation extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      collapse : false
    }
  }
  onClickCollapseMenu(e){
    e.preventDefault()
    this.setState({ collapse: !this.state.collapse })
  }
  render () {
    return(
      <nav className="navbar navbar-default">
        <div className="container">
            <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed" onClick={this.onClickCollapseMenu.bind(this)}>
                    <span className="sr-only">Toggle navigation</span> Menu <i className="fa fa-bars"></i>
                </button>
                <a className="navbar-brand page-scroll" href="#page-top">Design Element</a>
            </div>

            <div className={classnames("collapse navbar-collapse", { 'in' : this.state.collapse})}>
                <ul className="nav navbar-nav navbar-right">
                    <li>
                      <Link to="/components/design/design-components" className="nav-link">Design Elements</Link>
                    </li>
                    <li>
                        <Link to="/components/ui/ui-components" className="nav-link">UI-Components</Link>
                    </li>
                    <li>
                      <Link to="/components/js/js-components" className="nav-link">JS-Components</Link>
                    </li>
                    <li>
                      <Link to="/components/scss/scss-components" className="nav-link">SCSS</Link>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    )
  }
}

export default Navigation;
