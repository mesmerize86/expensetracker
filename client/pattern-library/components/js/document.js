import React, { PropTypes } from 'react'

class Document extends React.Component {
  render () {
    return(
      <div>
        <h2>Naming Convention</h2>
        <hr/>
        <p>This document will help you to understand the naming convention for javascript, ES6 and react component.</p>

        <h4>React Component</h4>
        <p>If you create a new react component file then it should start with small letter. e.g. <strong>layout.js</strong>.<br/>
        However if the component has two words then it should separate with &ndash; e.g. <strong>pattern-library</strong></p>

        <h4>Import React Component</h4>
        <p>When you import React component, it must start with capital letter. e.g;<br/>
        <strong>import Layout from './layout'</strong><br/>
        <strong>import PatternLibrary from './pattern-library'</strong></p>

        <h4>Javascript Variable</h4>
        <p>All the variable should start with small letter and if there are two words, it should separate with -. e.g.<br/>
        <strong>const pattern = { }</strong><br/>
        <strong>const pattern-library = { }</strong>
        </p>

        <h4>Javascript function</h4>
        <p>All the function should start with small letter and camelCase. e.g.<br/>
        <strong>onSubmit()</strong>
        </p>
        <h1>Folder Structure</h1>
        <hr/>
        <p>- <strong>Component</strong></p>
        <span>This is global component that will be shared among entire application</span>
        <p>-- form-group</p>
        <p>- <strong>data</strong></p>
        <span>bridge between api server and client</span>
        <p>-- users</p>
        <p>--- action.js</p>
        <p>--- api.js</p>
        <p>--- reducer.js</p>
        <p>- <strong>pages</strong></p>
          <p>- layout</p>
          <span>all the components related to layout. This component can be shared among only pages components</span>
          <p>&nbsp;&nbsp;--- header</p>
          <p>&nbsp;&nbsp;--- nav</p>
          <p>&nbsp;&nbsp;--- sidebar</p>
          <p>&nbsp;&nbsp;--- footer</p>
          <p>- page-name</p>
          <p>&nbsp;&nbsp;--- home</p>
          <p>&nbsp;&nbsp;--- register</p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;---- form</p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----- login</p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----- signup</p>
          <span>page specific component or content placeholder for specific page.</span>
        <p>- <strong>services</strong></p>
        <span>similar to data.</span>
        <p>- <strong>index.js</strong></p>
        <p>- <strong>store.js</strong></p>
    </div>
)
  }
}

export default Document;
