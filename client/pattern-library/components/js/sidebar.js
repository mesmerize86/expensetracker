import React from 'react'
import { Link } from 'react-router'

class Sidebar extends React.Component {
  render () {
    return(
      <div className="sidebar">
        <label>JS Components</label>
        <ul className="sidebar-nav">
          <li><Link to="/components/js/document" className="sidebar-nav-link">Naming Convention</Link></li>
          <li><Link to="/components/js/document" className="sidebar-nav-link">Folder Structure</Link></li>
        </ul>
      </div>
    )
  }
}

export default Sidebar
