import React from 'react'
import Sidebar from './sidebar'

class DesignComponent extends React.Component {
  render () {
    return(
      <div className="container">
        <div className="row">
          <div className="col-xs-3">
            <Sidebar />
          </div>
          <div className="col-xs-9">
            <div className="content">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default DesignComponent;
