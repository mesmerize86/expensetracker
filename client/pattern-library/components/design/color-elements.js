import React from 'react'

class ColorElements extends React.Component {
  render () {
    return(
        <div className="content">
          <h3>Colour Palette</h3>
          <p>This is the primary and secondary colour palettes as described in the PSD styleguide and in theme.sass</p>
          <div className="panel panel-default">
            <div className="panel-body">
              <h4>Mono Palette</h4>
              <span className="swatch swatch--mono-xxxlight">$color-mono-xxxlight</span>
              <span className="swatch swatch--mono-xxlight">$color-mono-xxlight</span>
              <span className="swatch swatch--mono-xlight">$color-mono-xlight</span>
              <span className="swatch swatch--mono-light">$color-mono-light</span>
              <span className="swatch swatch--mono">$color-mono</span>
              <span className="swatch swatch--mono-dark">$color-mono-dark</span>
              <span className="swatch swatch--mono-xdark">$color-mono-xdark</span>
              <span className="swatch swatch--mono-xxdark">$color-mono-xxdark</span>
              <span className="swatch swatch--mono-xxxdark">$color-mono-xxxdark</span>

              <h4>Primary Palette</h4>
              <span className="swatch swatch--primary-xlight">$theme-color-primary-xlight</span>
              <span className="swatch swatch--primary-light">$theme-color-primary-light</span>
              <span className="swatch swatch--primary">$theme-color-primary</span>
              <span className="swatch swatch--primary-dark">$theme-color-primary-dark</span>
              <br/>
                <span className="swatch swatch--primary-red">$theme-color-primary-red</span>
                <span className="swatch swatch--primary-green">$theme-color-primary-green</span>
                <span className="swatch swatch--primary-yella">$theme-color-primary-yella</span>
                <span className="swatch swatch--primary-orange">$theme-color-primary-orange</span>
                <span className="swatch swatch--primary-plum">$theme-color-primary-plum</span>
                
            <h4>Type</h4>
            <span className="swatch swatch--body-copy">$theme-color-body-copy</span>
            </div>
          </div>
      </div>
    )
  }
}

export default ColorElements
