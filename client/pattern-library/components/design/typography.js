import React from 'react'

class Typography extends React.Component {
  render () {
    return(
      <div className="content">
      <h4>Variables</h4>
      <p>These are some of core variables used for defining colors and font sizes accross .SCSS files. Do not override or change the styling of typographic base elements. Font sizes, colors, faces and margins should be consistant accross the entire site.</p>
      <h4>Typography</h4>
      <p>We achieve styling through opt in classes and do not style elements globally. This is to reduced the number of style collisions across projects. </p>
      <p>Typographic elements do not come with any margins attached to them in order to make them more flexible in where they can be used.</p>
      <p>They get margins when they are inside a wrapper with a class of context--content. We may need to work on the naming but the principle is solid.</p>
        <div className="panel panel-default">
          <div className="panel-body">
          <h4>Font Sizes</h4>
          <hr/>
            <div className="typeface typeface-xxxlarge">$font-size-xxxlarge [48px]</div>
            <div className="typeface typeface-xxlarge">$font-size-xxlarge [36px]</div>
            <div className="typeface typeface-xlarge">$font-size-xlarge [30px]</div>
            <div className="typeface typeface-large">$font-size-large [24px]</div>
            <div className="typeface typeface-base">$font-size-base [16px]</div>
            <div className="typeface typeface-normal">$font-size-normal [14px]</div>
            <div className="typeface typeface-small">$font-size-small [12px]</div>
            <div className="typeface typeface-xsmall">$font-size-xsmall [10px]</div>
            <div className="typeface typeface-xxsmall">$font-size-xxsmall [9px]</div>
            <br/>

            <h4>Headings</h4>
            <hr/>
            <table className="patternLibraryPage-table">
                <tr>
                    <td>
                        <h1>Heading 1</h1></td>
                    <td><pre>&lt;h1> Heading 1 &lt;/h1></pre></td>
                </tr>
                <tr>
                    <td>
                        <h2>Heading 2</h2></td>
                    <td><pre>&lt;h2> Heading 2 &lt;/h2></pre></td>
                </tr>
                <tr>
                    <td>
                        <h3>Heading 3</h3></td>
                    <td><pre>&lt;h3> Heading 3 &lt;/h3></pre></td>
                </tr>
                <tr>
                    <td>
                        <h4>Heading 4</h4></td>
                    <td><pre>&lt;h4> Heading 4 &lt;/h4></pre></td>
                </tr>
                <tr>
                    <td>
                        <h5>Heading 5</h5></td>
                    <td><pre>&lt;h5> Heading 5 &lt;/h5></pre></td>
                </tr>
                <tr>
                    <td>
                        <h6>Heading 6</h6></td>
                    <td><pre>&lt;h6> Heading 6 &lt;/h6></pre></td>
                </tr>
                <tr>
                    <td><small>Small</small></td>
                    <td><pre>&lt;small> Small &lt;/small></pre></td>
                </tr>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

export default Typography;
