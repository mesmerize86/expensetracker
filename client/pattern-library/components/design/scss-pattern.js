import React from 'react'

class SCSSPattern extends React.Component {
  render () {
    return(
      <div>
        <p><strong>NAMESPACING</strong></p> 
        <p>I am using a namespacing that is based off the BEM front-end naming methodology which stands for: Block, Element, Modifyer. The naming convention follows the following pattern:</p>
        <p>.block{} <br/>
        .block__element{} <br/>
        .block--modifyer{}<br/>
          •	.block represents the high level element of the module<br/>
          •	.block__element represents a descendent of .block<br/>
          •	.block--modifyer represents a different version of .block<br/>
        The point of BEM is to tell other developers more about what a piece of markup is doing from its name alone.
</p>
      </div>
    )
  }
}

export default SCSSPattern;