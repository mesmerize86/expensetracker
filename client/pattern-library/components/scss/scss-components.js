import React from 'react'

import Sidebar from './sidebar'

class SCSSComponent extends React.Component {
  render () {
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-3">
            <Sidebar />
          </div>
          <div className="col-xs-9">
            <div className="content">
              <h1>SCSS Component</h1>
                <p><strong>NAMESPACING</strong></p>
                <p>I am using a namespacing that is based off the BEM front-end naming methodology which stands for: Block, Element, Modifyer. The naming convention follows the following pattern:</p>
                <p>.block{} <br/>
                .block__element{} <br/>
                .block--modifyer{}<br/>
                  •	.block represents the high level element of the module<br/>
                  •	.block__element represents a descendent of .block<br/>
                  •	.block--modifyer represents a different version of .block<br/>
                The point of BEM is to tell other developers more about what a piece of markup is doing from its name alone.
                </p>
                <h1>Folder Structure</h1>
                <p>SMACSS Architecture</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default SCSSComponent;
