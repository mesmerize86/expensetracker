import React, { PropTypes } from 'react'

class FormGroup extends React.Component {
  render () {
    return(
      <div>
        <h1>Forms</h1>
        <hr />
        <h3 id="form-example">Basic Example</h3>
        <div className="panel panel-default">
          <div className="panel-body">
            <form>
              <div className="form-group">
                <label className='form-label'>Username</label>
                <input type='text' name='username' className='form-control'/>
              </div>
              <div className="form-group">
                <label className='form-label'>Password</label>
                <input type='password' name='password' className='form-control'/>
              </div>
              <button type="submit" className="button button-primary button-block">Login</button>
            </form>
            <pre><code>&lt;form&gt;<br/>
              &nbsp;&nbsp;&lt;div className=&quot;form-group&quot;&gt;<br/>
                &nbsp;&nbsp;&nbsp;&lt;label className='form-label'&gt;Username&lt;/label&gt;<br/>
                &nbsp;&nbsp;&nbsp;&lt;input type='text' name='username' className='form-control'/&gt;<br/>
                  &nbsp;&nbsp;&lt;/div&gt;<br/>
                  &nbsp;&nbsp;&lt;div className=&quot;form-group&quot;&gt;<br/>
                &nbsp;&nbsp;&nbsp;&lt;label className='form-label'&gt;Password&lt;/label&gt;<br/>
                  &nbsp;&nbsp;&nbsp;&lt;input type='password' name='password' className='form-control'/&gt;<br/>
                  &nbsp;&nbsp;&lt;/div&gt;<br/>
                &nbsp;&nbsp;&lt;button type=&quot;submit&quot; className=&quot;button button-primary button-block&quot;&gt;Login&lt;/button&gt;<br/>
              &lt;/form&gt;<br/>
              </code></pre>
            </div>
          </div>
        <h3 id="horizontal-form">Horizontal Form</h3>
        <div className="panel panel-default">
          <div className="panel-body">
            <form className="form-horizontal">
              <div className="form-group">
                <label className='form-label'>Username</label>
                <input type='text' name='username' className='form-control'/>
              </div>
              <div className="form-group">
                <label className='form-label'>Password</label>
                <input type='password' name='password' className='form-control'/>
              </div>
              <button type="submit" className="button button-primary button-block">Login</button>
            </form>
            <pre>
              <code>
                &lt;form className=&quot;form-horizontal&quot;&gt;<br/>
                  &nbsp;&nbsp;&lt;div className=&quot;form-group&quot;&gt;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;label className='form-label'&gt;Username&lt;/label&gt;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type='text' name='username' className='form-control'/&gt;<br/>
                  &nbsp;&nbsp;&lt;/div&gt;<br/>
                  &nbsp;&nbsp;&lt;div className=&quot;form-group&quot;&gt;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;label className='form-label'&gt;Password&lt;/label&gt;<br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&lt;input type='password' name='password' className='form-control'/&gt;<br/>
                  &nbsp;&nbsp;&lt;/div&gt;<br/>
                  &nbsp;&nbsp;&lt;button type=&quot;submit&quot; className=&quot;button button-primary button-block&quot;&gt;Login&lt;/button&gt;<br/>
                &lt;/form&gt;
              </code>
            </pre>
          </div>
        </div>
    </div>
    )
  }
}

export default FormGroup;
