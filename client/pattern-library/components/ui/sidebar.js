import React from 'react'
import { Link } from 'react-router'

class Sidebar extends React.Component {
  render () {
    return(
      <div className="sidebar">
        <label>UI Components</label>
        <ul className="sidebar-nav">
          <li>
            <Link to="/components/ui/form-group" className="sidebar-nav-link">Form</Link>
            <a href="#form-example" className="sidebar-megamenu">Basic Example</a>
            <a href="#horizontal-form" className="sidebar-megamenu">Horizontal Form</a>
          </li>
          <li><Link to="/components/ui/button" className="sidebar-nav-link">Button</Link></li>
        </ul>
      </div>
    )
  }
}

export default Sidebar
