import React from 'react'

import Header from './layout/header'
import Navigation from './layout/navigation'
import MainContent from './layout/main-content'

class PatternLibrary extends React.Component {
  render () {
    return(
        <div className="patternLibraryPage Page">
          <div className="wrapper">
            <Header />
            <Navigation />
            <MainContent patternLibraryNav={this.props.children} />
          </div>
        </div>
    )
  }
}

export default PatternLibrary;
