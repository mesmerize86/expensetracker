import path from 'path';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin'

export default {
  entry: [
    'webpack-hot-middleware/client',
    path.join(__dirname, '/client/index.js'),
    path.join(__dirname, './themes/expensetracker/client/sass/main-sky.scss')
  ],
  output: {
    path: '/',
    publicPath: '/'
  },
  module: {
    devtools: 'eval-source-map',
    loaders: [
      {
        test: /\.(js|jsx)$/,
        include: [
          path.join(__dirname, 'server/shared/validation'),
          path.join(__dirname, 'client')
        ],
        loaders: [ 'react-hot', 'babel']
      },
      {
        test:/\.scss$/,
        loaders: ['style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader?sourceMap']
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url-loader?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.jpg?$/,
        loader: 'file-loader'
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', 'jsx']
  },
  plugins: [
    new webpack.NoErrorsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin(
    {
      filename: 'style.css',
      allChunks: true
    }
  )
  ]
}