module.exports = function(grunt){

	'use strict'
	//Load NPM Plugins
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	//Load Theme Config
	var configJson = grunt.file.readJSON('config.json');

	//Project Configuration
	grunt.initConfig({
		pkg:grunt.file.readJSON('package.json')
	});

	//grunt debug:themename:site:module
	//grunt release:themename:site:module
    //grunt.log.writeln(JSON.stringify(grunt.config(), null, 2));

	grunt.registerTask('debug', 'Watch frontend assets on DEBUG mode', registerWatchTask);
    grunt.registerTask('release', 'Watch frontend assets on RELEASE mode', registerWatchTask);

	var requestConfig = {};

	function registerWatchTask(theme, site, module){
		var mode = this.name.toLowerCase();
        
        requestConfig.theme = theme;
		requestConfig.site = site;
		requestConfig.module = module;
        requestConfig.isDebugMode = mode === 'debug';
		requestConfig.hasSite = site !== undefined;
		requestConfig.hasModule = module !== undefined;
		requestConfig.themeConfig = configJson[theme];

		var expenseTrackerConfig = require('./expenseTrackerConfig.js')(requestConfig, grunt);

		register(requestConfig.isDebugMode, requestConfig, expenseTrackerConfig);
        run(requestConfig.isDebugMode, requestConfig);
	}

	function register(isDebugMode,themeSiteConfig, expenseTrackerConfig){
		var gruntConfig = {};

		if(!themeSiteConfig.hasModule || themeSiteConfig.module == 'css'){
			gruntConfig.sass = expenseTrackerConfig.sass();
            gruntConfig.sync = expenseTrackerConfig.sync();
		}
        
        if(!themeSiteConfig.hasModule || themeSiteConfig.module == 'js'){
			gruntConfig.uglify = expenseTrackerConfig.uglify();
            gruntConfig.sync = expenseTrackerConfig.sync();
		}
        
        if(isDebugMode)
            gruntConfig.connect = expenseTrackerConfig.connect();
            
		gruntConfig.watch = expenseTrackerConfig.watch();
		grunt.config.merge(gruntConfig);
	}

	function run(isDebugMode, requestConfig){
        grunt.task.run(['sync']);
        
		if(!requestConfig.hasModule || requestConfig.module == 'css'){
			grunt.task.run(['sass']);
		}
        
        if(!requestConfig.hasModule || requestConfig.module == 'js'){
			grunt.task.run(['uglify']);
		}
        
        if(isDebugMode) {
            grunt.task.run(['connect', 'watch']);
        }
	}
};